package org.badcoders.aimsicd.model;

import org.badcoders.aimsicd.utils.Helpers;

/**
 * We often talk about "Network Type", when we actually refer to:
 * "RAN" = Radio Access Network (cellular communication only)
 * "RAT" = Radio Access Technology (any wireless communication technology, like WiMax etc.)
 *
 * As for this application, we shall use the terms:
 * "Type" for the text values like ( UMTS/WCDMA, HSDPA, CDMA, LTE etc)  and
 * "RAT" for the numerical equivalent (As obtained by AOS API?)
 */
public class Cell {

    private int cellId = -1;             // Cell Identification code
    private int lac = -1;                // Location Area Code
    private int mcc = -1;                // Mobile Country Code
    private int mnc = -1;                // Mobile Network Code
    private int dbm = -1;                // [dBm] RX signal "power"
    private int asu = -1;                // signal strength in ASU
    private int psc = -1;                // Primary Scrambling Code
    private int timingAdvance = -1;      // Timing Advance [LTE,GSM]
    private int sid  = -1;               // Cell-ID for [CDMA]
    private long firstSeen = 0l;
    private long lastSeen = 0l;
    private String technology = "NA";

    // Tracked Cell Specific Variables
    private int netType = -1;
    private double speed = 0.0d;
    private double accuracy = 0.0d;
    private double lon = 0.0d;
    private double lat = 0.0d;

    private int samples = 0;
    private String tmsi;
    private String source;

    public Cell() {
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
    }

    public int getCellId() {
        return this.cellId;
    }

    public void setCellId(int cid) {
        this.cellId = cid;
    }

    public int getLAC() {
        return this.lac;
    }

    public long getFirstSeen() {
        return firstSeen;
    }

    public void setFirstSeen(long firstSeen) {
        this.firstSeen = firstSeen;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public void setLAC(int lac) {
        this.lac = lac;
    }

    /**
     * Mobile Country Code (Mcc) of current Cell
     *
     * @return int Current cells Mobile Country Code
     */
    public int getMCC() {
        return this.mcc;
    }

    /**
     * Set Mobile Country Code (Mcc) of current Cell
     *
     * @param mcc
     *         Mobile Country Code
     */
    public void setMCC(int mcc) {
        this.mcc = mcc;
    }

    /**
     * Mobile Network Code (Mnc) of current Cell
     *
     * @return int Current cells Mobile Network Code
     */
    public int getMNC() {
        return this.mnc;
    }

    /**
     * Set Mobile Network Code (Mnc) of current Cell
     *
     * @param mnc
     *         Mobile Network Code
     */
    public void setMNC(int mnc) {
        this.mnc = mnc;
    }

    /**
     * Primary Scrambling Code (PSC) of current Cell
     *
     * @return int Current cells Primary Scrambling Code
     */
    public int getPSC() {
        return this.psc;
    }

    /**
     * Set Primary Scrambling Code (PSC) of current Cell
     *
     * @param psc
     *         Primary Scrambling Code
     */
    public void setPSC(int psc) {
        this.psc = psc;
    }

    /**
     * CDMA System ID
     *
     * @return System ID or -1 if not supported
     */
    public int getSID() {
        return this.sid;
    }

    /**
     * Set CDMA System ID (SID) of current Cell
     *
     * @param sid
     *         CDMA System ID
     */
    public void setSID(int sid) {
        this.sid = sid;
    }

    /**
     * Signal Strength Measurement (dBm)
     *
     * @return Signal Strength Measurement (dBm)
     */
    public int getDBM() {
        return dbm;
    }

    /**
     * Set Signal Strength (dBm) of current Cell
     *
     * @param dbm
     *         Signal Strength (dBm)
     */
    public void setDBM(int dbm) {
        this.dbm = Helpers.getDbm(dbm);
    }

    /**
     * Longitude Geolocation of current Cell
     *
     * @return Longitude
     */
    public double getLon() {
        return this.lon;
    }

    /**
     * Set Longitude Geolocation of current Cell
     *
     * @param lon
     *         Longitude
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * Latitude Geolocation of current Cell
     *
     * @return Latitude
     */
    public double getLat() {
        return this.lat;
    }

    /**
     * Set Latitude Geolocation of current Cell
     *
     * @param lat
     *         Latitude
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * Ground speed in metres/second
     *
     * @return Ground speed or 0.0 if unavailable
     */
    public double getSpeed() {
        return this.speed;
    }

    /**
     * Set current ground speed in metres/second
     *
     * @param speed
     *         Ground Speed
     */
    public void setSpeed(double speed) {
        this.speed = speed;
    }

    /**
     * Accuracy of location in metres
     *
     * @return Location accuracy in metres or 0.0 if unavailable
     */
    public double getAccuracy() {
        return this.accuracy;
    }

    /**
     * Set current location accuracy in metres
     *
     * @param accuracy
     *         Location accuracy
     */
    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    /**
     * LTE Timing Advance
     *
     * @return LTE Timing Advance or -1 if unavailable
     */
    public int getTimingAdvance() {
        return this.timingAdvance;
    }

    /**
     * Set current LTE Timing Advance
     *
     * @param ta
     *         Current LTE Timing Advance
     */
    public void setTimingAdvance(int ta) {
        this.timingAdvance = ta;
    }

    /**
     * Network Type
     *
     * @return string representing device Network Type
     */
    public int getNetType() {
        return this.netType;
    }

    /**
     * Set current Network Type
     *
     * @param netType
     *         Current Network Type
     */
    public void setNetType(int netType) {
        this.netType = netType;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(((Object) this).getClass() != obj.getClass()) {
            return false;
        }
        //@formatter:off
        Cell other = (Cell) obj;
        if(this.psc != -1) {
            return this.cellId == other.getCellId()
                   && this.lac == other.getLAC()
                   && this.mcc == other.getMCC()
                   && this.mnc == other.getMNC()
                   && this.psc == other.getPSC();
        } else {
            return this.cellId == other.getCellId()
                   && this.lac == other.getLAC()
                   && this.mcc == other.getMCC()
                   && this.mnc == other.getMNC();
        }//@formatter:on
    }

    public boolean isValid() {
        return this.getCellId() != -1 && this.getLAC() != -1;
    }

    public String getTmsi() {
        return this.tmsi;
    }

    public void setTmsi(String tmsi) {
        this.tmsi = tmsi;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return this.source;
    }

    public int getAsu() {
        return asu;
    }

    public void setAsu(int asu) {
        this.asu = asu;
    }
}
