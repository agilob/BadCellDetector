package org.badcoders.aimsicd.model;

import android.telephony.TelephonyManager;

import org.acra.ACRA;
import org.badcoders.aimsicd.R;

public class Device {

    private int mPhoneID = -1;
    private String operatorName;
    private String networkOperatorCode;
    private String mSimCountry;
    private String mPhoneType;
    private String mIMEI;
    private String mIMEIV;
    private String mSimOperator;
    private String mSimOperatorName;
    private String mSimSerial;
    private String mSimSubs;
    private int mDataActivityType;
    private boolean mRoaming;

    /**
     * Refreshes all device specific details
     */
    public void refreshDeviceInfo(TelephonyManager tm) {
        //Phone type and associated details
        mIMEI = tm.getDeviceId();
        mIMEIV = tm.getDeviceSoftwareVersion();
        mPhoneID = tm.getPhoneType();
        mRoaming = tm.isNetworkRoaming();
        networkOperatorCode = tm.getNetworkOperator();
        operatorName = tm.getNetworkOperatorName();

        switch(mPhoneID) {
            case TelephonyManager.PHONE_TYPE_GSM:
                mPhoneType = "GSM";
                break;

            case TelephonyManager.PHONE_TYPE_CDMA:
                mPhoneType = "CDMA";
                break;
        }

        // SIM Information
        if(tm.getSimState() == TelephonyManager.SIM_STATE_READY) {
            mSimCountry = getSimCountry(tm);
            // Get the operator code of the active SIM (MCC + MNC)
            mSimOperator = tm.getSimOperator();
            mSimOperatorName = tm.getSimOperatorName();
            mSimSerial = getSimSerial(tm);
            mSimSubs = getSimSubs(tm);
        }

        mDataActivityType = getDataActivity(tm);
    }

    public int getPhoneID() {
        return mPhoneID;
    }

    public String getSimCountry(TelephonyManager tm) {
        try {
            if(tm.getSimState() == TelephonyManager.SIM_STATE_READY) {
                mSimCountry = (tm.getSimCountryIso() != null) ? tm.getSimCountryIso().toUpperCase() : "N/A";
            } else {
                mSimCountry = "N/A";
            }
        } catch(Exception e) {
            // SIM methods can cause Exceptions on some devices
            mSimCountry = "N/A";
            ACRA.getErrorReporter().handleSilentException(e);
        }

        if(mSimCountry.isEmpty()) {
            mSimCountry = "N/A";
        }

        return mSimCountry;
    }

    public String getSimCountry() {
        return mSimCountry;
    }

    /**
     * SIM Operator
     *
     */
    public String getSimOperator() {
        return mSimOperator;
    }

    public String getSimOperatorName() {
        return mSimOperatorName;
    }

    /**
     * SIM Subscriber ID
     *
     * @return string of SIM Subscriber ID data
     */
    public String getSimSubs(TelephonyManager tm) {
        try {
            if(tm.getSimState() == TelephonyManager.SIM_STATE_READY) {
                mSimSubs = tm.getSubscriberId() != null ? tm.getSubscriberId() : "N/A";
            } else {
                mSimSubs = "N/A";
            }
        } catch(Exception e) {
            //Some devices don't like this method
            mSimSubs = "N/A";
            ACRA.getErrorReporter().handleSilentException(e);
        }

        if(mSimSubs.isEmpty()) {
            mSimSubs = "N/A";
        }

        return mSimSubs;
    }

    public String getSimSubs() {
        return mSimSubs;
    }

    /**
     * SIM Serial Number
     */
    public String getSimSerial(TelephonyManager tm) {
        try {
            if(tm.getSimState() == TelephonyManager.SIM_STATE_READY) {
                mSimSerial = tm.getSimSerialNumber() != null ? tm.getSimSerialNumber() : "N/A";
            } else {
                mSimSerial = "N/A";
            }
        } catch(Exception e) {
            // SIM methods can cause Exceptions on some devices
            mSimSerial = "N/A";
            ACRA.getErrorReporter().handleSilentException(e);
        }

        if(mSimSerial.isEmpty()) {
            mSimSerial = "N/A";
        }

        return mSimSerial;
    }

    public String getSimSerial() {
        return mSimSerial;
    }

    public String getPhoneType() {
        return mPhoneType;
    }

    public String getIMEI() {
        return mIMEI;
    }

    public String getIMEIv() {
        return mIMEIV;
    }

    public String getNetworkOperatorName() {
        return operatorName;
    }

    public String getOperatorCode() {
        return networkOperatorCode;
    }

    public int getDataActivity(TelephonyManager tm) {
        int direction = tm.getDataActivity();

        switch(direction) {
            case TelephonyManager.DATA_ACTIVITY_NONE:
                return R.string.data_activity_none;
            case TelephonyManager.DATA_ACTIVITY_IN:
                return R.string.data_activity_in;
            case TelephonyManager.DATA_ACTIVITY_OUT:
                return R.string.data_activity_out;
            case TelephonyManager.DATA_ACTIVITY_INOUT:
                return R.string.data_activity_in_out;
            case TelephonyManager.DATA_ACTIVITY_DORMANT:
                return R.string.data_activity_dormant;
            default:
                return R.string.unknown;
        }
    }

    public int getDataActivity() {
        return mDataActivityType;
    }

    public void setDataActivityType(int dataActivityType) {
        mDataActivityType = dataActivityType;
    }

    public boolean isRoaming() {
        return mRoaming;
    }

}
