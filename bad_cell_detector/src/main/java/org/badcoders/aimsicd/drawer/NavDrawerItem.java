package org.badcoders.aimsicd.drawer;

public interface NavDrawerItem {

    String getLabel();

    void setLabel(String label);

    int getId();

    void setId(int pId);

    void setIconId(int icon);

    int getType();

    boolean isEnabled();
}
