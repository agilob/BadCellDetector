package org.badcoders.aimsicd.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

public class WiFiStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

        if(networkInfo != null &&
                   networkInfo.getType() == ConnectivityManager.TYPE_WIFI &&
                   networkInfo.getState() == NetworkInfo.State.CONNECTED) {
        }
    }
}
