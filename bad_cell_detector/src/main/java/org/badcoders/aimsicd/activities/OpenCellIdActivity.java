package org.badcoders.aimsicd.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.service.AimsicdService;
import org.badcoders.aimsicd.tasks.OpenCellIdKeyDownloadTask;
import org.badcoders.aimsicd.utils.NetworkUtil;

import java.util.concurrent.ExecutionException;

/**
 * Popup toast messages asking if user wants to download new API key to access OpenCellId services
 * and data.
 */
public class OpenCellIdActivity extends FragmentActivity {

    private static final String TAG = "OpenCellIdActivity";

    private SharedPreferences prefs;
    private ProgressDialog pd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_open_cell_id);
        prefs = getSharedPreferences(AimsicdService.SHARED_PREFERENCES_BASENAME, 0);
    }

    public void onAcceptedClicked(View v) {
        if(NetworkUtil.isInternetConnectionAvailable(this)) {
            pd = new ProgressDialog(this);
            pd.setMessage(getString(R.string.getting_ocid_key));
            pd.show();

            OpenCellIdKeyDownloadTask ocikd = new OpenCellIdKeyDownloadTask(this, prefs, pd);
            try {
                ocikd.execute().get(); //starts background thread
            } catch(InterruptedException e) {
                Log.e(TAG, e.getClass().toString(), e);
            } catch(ExecutionException e) {
                Log.e(TAG, e.getClass().toString(), e);
            }
        } else {
            Toast.makeText(this, R.string.please_enable_network_connectivity,
                           Toast.LENGTH_LONG).show();
        }
        finish();
    }

    public void onCancelClicked(View v) {
        finish();
    }
}
