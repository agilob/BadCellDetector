package org.badcoders.aimsicd.activities;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.utils.MyNotification;

public class CustomPopUp extends Activity {

    private TextView tv_popup_title, about_tv_status, about_tv_data;
    private ImageView about_icon_holder;
    private int mode = 0; //default

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.about_pop_up);

        about_icon_holder = (ImageView) findViewById(R.id.about_icon_holder);
        about_tv_status = (TextView) findViewById(R.id.about_tv_status);
        about_tv_data = (TextView) findViewById(R.id.about_tv_data);
        tv_popup_title = (TextView) findViewById(R.id.tv_popup_title);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mode = extras.getInt("display_mode");
        }

        setFinishOnTouchOutside(true);
        switch(mode) {
            case 0:
                createPopUp(
                        null,
                        getString(R.string.status) + "\t" + getString(R.string.idle),
                        getResources().getString(R.string.detail_info_idle),
                        getResources().getDrawable(R.drawable.white_idle));
                break;
            case 1:
                createPopUp(
                        null,
                        getString(R.string.status) + "\t" + getString(R.string.normal),
                        getResources().getString(R.string.detail_info_normal),
                        getResources().getDrawable(R.drawable.white_ok));
                break;
            case 2:
                createPopUp(
                        null,
                        getString(R.string.status) + "\t" + getString(R.string.medium),
                        getResources().getString(R.string.detail_info_medium),
                        getResources().getDrawable(R.drawable.white_medium));
                break;
            case 3:
                createPopUp(
                        null,
                        getString(R.string.status) + "\t" + getString(R.string.high),
                        getResources().getString(R.string.detail_info_high),
                        getResources().getDrawable(R.drawable.white_high));
                break;
            case 4:
                createPopUp(
                        null,
                        getString(R.string.status) + "\t" + getString(R.string.danger),
                        getResources().getString(R.string.detail_info_danger),
                        getResources().getDrawable(R.drawable.white_danger));
                break;
            case 5:
                createPopUp(
                        null,
                        getString(R.string.status) + "\t" + getString(R.string.run),
                        getResources().getString(R.string.detail_info_run),
                        getResources().getDrawable(R.drawable.white_skull));
                break;
            case 6:
                new MyNotification(getApplicationContext(),
                                   getResources().getString(R.string.alert_silent_sms_detected),
                                   getResources().getString(
                                           R.string.app_name_short) + " - " + getResources().getString(
                                           R.string.typezero_header));
                createPopUp(
                        getResources().getString(R.string.typezero_title),
                        getResources().getString(R.string.typezero_header),
                        getResources().getString(R.string.typezero_data),
                        getResources().getDrawable(R.drawable.white_danger));
                break;
            case 7:
                new MyNotification(getApplicationContext(),
                                   getResources().getString(R.string.alert_mwi_detected),
                                   getResources().getString(
                                           R.string.app_name_short) + " - " + getResources().getString(
                                           R.string.typemwi_header));
                createPopUp(
                        getResources().getString(R.string.typemwi_title),
                        getResources().getString(R.string.typemwi_header),
                        getResources().getString(R.string.typemwi_data),
                        getResources().getDrawable(R.drawable.white_danger));
                break;
            case 8:
                new MyNotification(getApplicationContext(),
                                   getResources().getString(R.string.alert_silent_wap_sms_detected),
                                   getResources().getString(
                                           R.string.app_name_short) + " - " + getResources().getString(
                                           R.string.typewap_header));
                createPopUp(
                        getResources().getString(R.string.typemwi_title),
                        getResources().getString(R.string.typewap_header),
                        getResources().getString(R.string.typewap_data),
                        getResources().getDrawable(R.drawable.white_danger));
                break;
            case 9:
                createPopUp(
                        getResources().getString(R.string.dependencies),
                        getResources().getString(R.string.main_app_name),
                        getResources().getString(R.string.list_of_dependencies) +
                                "\norg.apache.commons:commons-collections\n" +
                                "org.osmdroid:osmdroid-android\n" +
                                "osmbonuspack\n" +
                                "com.github.Stericson:RootShell\n" +
                                "com.github.kevinsawicki:http-request\n" +
                                "com.google.code.gson:gson\n" +
                                "au.com.bytecode:opencsv\n" +
                                "com.jjoe64:graphview\n" +
                                "ch.acra:acra\n" +
                                "org.sonarqube",
                        getResources().getDrawable(R.drawable.white_ok));
                break;
        }
    }

    public void createPopUp(String title, String header, String data, Drawable icon) {
        if(title != null) {
            tv_popup_title.setText(title);
        }
        if(header != null) {
            about_tv_status.setText(header);
        }
        if(data != null) {
            about_tv_data.setText(data);
        }
        if(icon != null) {
            about_icon_holder.setImageDrawable(icon);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if(mode == 6 || mode == 7 || mode == 8) {
                    new MyNotification(getApplicationContext(),
                                       getResources().getString(R.string.app_name_short),
                                       getResources().getString(
                                               R.string.app_name_short) + " - " + getResources().getString(
                                               R.string.status_good));
                }
                finish();
        }
        return true;
    }
}
