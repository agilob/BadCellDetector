package org.badcoders.aimsicd.tasks;

import android.os.AsyncTask;

import com.github.kevinsawicki.http.HttpRequest;

import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.service.CellTracker;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class OpenCellIdLookUpTask extends AsyncTask<Cell, Integer, Integer> {

    private static final String TAG = "OpenCellIdLookUpTask";

    protected List<Cell> cells;

    @Override
    protected Integer doInBackground(Cell... params) {
        String urlRequest = buildRequest(params[0]);
        if(urlRequest == null) {
            return -1;
        }
        int code;
        try {
            OpenCellIdCellLookupParser parser = new OpenCellIdCellLookupParser();

            File file = null;
            HttpRequest request = HttpRequest.get(urlRequest);
            code = request.code();

            if(code == 200) {
                file = File.createTempFile("ocidLookupFile", ".tmp");
                request.receive(file);
                cells = parser.parse(file);
            }
            if(file != null) {
                file.deleteOnExit();
            }

        } catch(RuntimeException | IOException exp) {
            return -1000;
        }
        return code;
    }

    private String buildRequest(Cell cell) {
        if(cell.getCellId() == -1) {
            return null;
        }
        String url = "http://www.opencellid.org/cell/get?key=";
        url += CellTracker.OCID_API_KEY;

        if(cell.getCellId() != -1) {
            url += "&cellid=" + cell.getCellId();
        } else {
            return null;
        }
        if(cell.getMCC() != -1) {
            url += "&mcc=" + cell.getMCC();
        }
        if(cell.getMNC() != -1) {
            url += "&mnc=" + cell.getMNC();
        }
        url += "&lac=" + cell.getLAC();
        url += "&format=xml";
        return url;
    }
}

