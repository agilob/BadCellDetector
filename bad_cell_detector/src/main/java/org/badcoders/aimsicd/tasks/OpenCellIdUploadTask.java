package org.badcoders.aimsicd.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;

import org.badcoders.aimsicd.Aimsicd;
import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.service.CellTracker;

import java.io.File;
import java.io.IOException;

public class OpenCellIdUploadTask extends AsyncTask<String, Integer, Integer> {

    private static final String TAG = "OpenCellIdUploadTask";

    private Context mContext;
    private DatabaseAdapter dbAdapter;

    public OpenCellIdUploadTask(Context context, DatabaseAdapter dbAdapter) {
        this.mContext = context;
        this.dbAdapter = dbAdapter;
    }

    @Override
    protected Integer doInBackground(String... params) {

        if(dbAdapter.prepareOpenCellUploadData()) {
            File file;
            try {
                file = File.createTempFile("aimsicd-ocid-data", "csv");
            } catch(IOException io) {
                Log.e(TAG, "IO =>" + io.toString());
                publishProgress(0, 100);
                return -1000;
            }

            publishProgress(20, 100);
            file.deleteOnExit();

            try {
                HttpRequest request = HttpRequest.post(
                        "http://www.opencellid.org/measure/uploadCsv?key=" + CellTracker.OCID_API_KEY);
                request.part("datafile", "aimsicd-ocid-data.csv", file);
                publishProgress(60, 100);
                return request.code();
            } catch(RuntimeException re) {
                Log.e(TAG, "RunTimeException: " + re.getMessage(), re);
                return -1000;
            }
        } else {
            return -1;
        }
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 200) {
            Toast.makeText(mContext, mContext.getString(R.string.uploaded_bts_data_successfully),
                           Toast.LENGTH_SHORT).show();
        } else if(result == -1) {
            Toast.makeText(mContext, mContext.getString(R.string.no_data_for_publishing),
                           Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, mContext.getString(
                    R.string.unknown_problem_connecting_to_online_resource_encountered),
                           Toast.LENGTH_SHORT).show();
        }

        // just for visualisation
        Aimsicd.mProgressBar.setMax(100);
        Aimsicd.mProgressBar.setProgress(100);
        Aimsicd.mProgressBar.setProgress(0);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Aimsicd.mProgressBar.setProgress(values[0]);
        Aimsicd.mProgressBar.setMax(values[1]);
    }

}
