package org.badcoders.aimsicd.smsdetection;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import org.acra.ACRA;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.badcoders.aimsicd.activities.CustomPopUp;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.service.AimsicdService;
import org.badcoders.aimsicd.utils.Formatter;
import org.badcoders.aimsicd.utils.Helpers;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * Detects mysterious SMS by scraping Logcat entries.
 * <p/>
 * NOTES:   For this to work better Samsung users might have to set their Debug Level to High
 * in SysDump menu *#9900# or *#*#9900#*#*
 * <p/>
 * This is by no means a complete detection method but gives us something to work off.
 * <p/>
 * For latest list of working phones/models, please see:
 * https://github.com/SecUpwN/Android-IMSI-Catcher-Detector/issues/532
 * <p/>
 * PHONE:Samsung S5      MODEL:SM-G900F      ANDROID_VER:4.4.2   TYPE0:YES MWI:YES
 * PHONE:Samsung S4-min  MODEL:GT-I9195      ANDROID_VER:4.2.2   TYPE0:YES MWI:YES
 * PHONE:Sony Xperia J   MODEL:ST260i        ANDROID_VER:4.1.2   TYPE0:NO  MWI:YES
 * <p/>
 * To Use:
 * <p/>
 * SmsDetector smsdetector = new SmsDetector(context);
 * <p/>
 * smsdetector.startSmsDetection();
 * smsdetector.stopSmsDetection();
 * <p/>
 */
public class SmsDetector extends Thread {

    private final static String TAG = "SmsDetector";

    private static final int
            TYPE0 = 1,
            MWI = 2,
            WAP = 3;

    private static final int QUEUE_SIZE = 50;

    private BufferedReader mLogcatReader;
    private AimsicdService mAIMSICDService;
    private DatabaseAdapter mDbAdapter;
    private Context mContext;
    private Set<String> LOADED_DETECTION_STRINGS;
    private boolean mBound;

    private static boolean IS_RUNNING = false;

    public SmsDetector(Context context) {
        mContext = context;
        mDbAdapter = new DatabaseAdapter(context);

        List<AdvanceUserItems> silent_string = mDbAdapter.getDetectionStrings();

        LOADED_DETECTION_STRINGS = new HashSet<>();
        for(AdvanceUserItems advanceUserItems : silent_string) {
            LOADED_DETECTION_STRINGS.add((advanceUserItems.getDetectionString() +
                                                  "#" + advanceUserItems.getDetectionType()).toUpperCase());
        }
        LOADED_DETECTION_STRINGS.add("TYPE0#DETECTED!");
    }

    public static boolean getSmsDetectionState() {
        return IS_RUNNING;
    }

    public void startSmsDetection() {
        Intent intent = new Intent(mContext, AimsicdService.class);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        start();
        Log.i(TAG, "SMS detection started");
    }

    public void stopSmsDetection() {
        SmsDetector.IS_RUNNING = false;
        // Unbind from the service
        if(mBound) {
            mContext.unbindService(mConnection);
            mBound = false;
        }
        Log.i(TAG, "SMS detection stopped");
    }

    @Override
    public void run() {
        Log.i(TAG, "String logging thread");
        SmsDetector.IS_RUNNING = true;

        try {
            sleep(2000);

            String MODE = "logcat -v time -b radio -b main\n";
            Runtime r = Runtime.getRuntime();
            Process process = r.exec("su");
            DataOutputStream dos = new DataOutputStream(process.getOutputStream());

            dos.writeBytes(MODE);
            dos.flush();
            dos.close();

            mLogcatReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            Log.d(TAG, "Reading logcat");
        } catch(InterruptedException | IOException e) {
            ACRA.getErrorReporter().handleSilentException(e);
            SmsDetector.IS_RUNNING = false;
            Log.e(TAG, "Exception while initializing LogCat (time, radio, main) reader", e);
            return;
        }

        Queue<String> queue = new CircularFifoQueue<>(QUEUE_SIZE);

        try {
            while(SmsDetector.IS_RUNNING) {
                String bufferedLine;
                while((bufferedLine = mLogcatReader.readLine()) != null
                              && !bufferedLine.isEmpty()
                              // anything.package.name < prevents application package to be checked
                              && !bufferedLine.contains(TAG)) {
                    bufferedLine = bufferedLine.toUpperCase();
                    queue.add(bufferedLine);

                    switch(checkForSms(bufferedLine)) {
                        case TYPE0:
                            parseTypeZeroSms(bufferedLine,
                                             Formatter.logcatTimeStampParser(bufferedLine));
                            break;
                        case MWI:
                            parseMwiSms(bufferedLine,
                                        Formatter.logcatTimeStampParser(bufferedLine));
                            break;
                        case WAP:
                            parseWapPushSms(queue,
                                            Formatter.logcatTimeStampParser(bufferedLine));
                            /**
                             * we need to go forward a few more lines to get data
                             * and store it in post buffer array
                             * */
//                                    String[] wapPostLines = new String[QUEUE_SIZE];
//                                    for(int x = 0; x < QUEUE_SIZE; x++) {
//                                        String extraLine;
//                                        if((extraLine = mLogcatReader.readLine()) != null) {
//                                            wapPostLines[x] = extraLine;
//                                        }
//                                    }
                    }
                    break;
                }
            }
        } catch(IOException e) {
            ACRA.getErrorReporter().handleSilentException(e);
            Log.e(TAG, "IO Exception", e);
        }

        if(mLogcatReader != null) {
            try {
                mLogcatReader.close();
            } catch(IOException e) {
                ACRA.getErrorReporter().handleSilentException(e);
                Log.e(TAG, "IOE Error closing BufferedReader", e);
            }
        }
    }

    private int checkForSms(String line) {
        //0 - null 1 = TYPE0, 2 = MWI, 3 = WAPPUSH
        for(String LOADED_DETECTION_STRING : LOADED_DETECTION_STRINGS) {
            if(!LOADED_DETECTION_STRING.contains("#"))
                continue;

            String[] splitDetectionString = LOADED_DETECTION_STRING.split("#");
            if(line.contains(splitDetectionString[0])) {
                Log.v(TAG, "Line content: " + line);
                if("TYPE0".equals(splitDetectionString[1])) {
                    Log.e(TAG, "TYPE0 detected");
                    return TYPE0;
                } else if("MWI".equals(splitDetectionString[1])) {
                    Log.e(TAG, "MWI detected");
                    return MWI;
                } else if("WAPPUSH".equals(splitDetectionString[1])) {
                    Log.e(TAG, "WAPPUSH detected");
                    return WAP;
                }
            }
        }
        return 0;
    }

    private void parseTypeZeroSms(String bufferLine, Long logcat_timestamp) {

        CapturedSmsData capturedSms = new CapturedSmsData();
        String smsText = findSmsText(bufferLine);
        String num = findSmsNumber(bufferLine);

        capturedSms.setSenderNumber(num == null ? "null" : num);
        capturedSms.setSenderMsg(smsText == null ? "null" : num);
        capturedSms.setSmsTimestamp(logcat_timestamp);
        capturedSms.setSmsType("TYPE0");
        capturedSms.setLAC(mAIMSICDService.getCellTracker().getCell().getLAC());
        capturedSms.setCellId(mAIMSICDService.getCellTracker().getCell().getCellId());
        capturedSms.setTechnology(Helpers.getNetworkTypeName(mAIMSICDService.getCell().getNetType()));
        capturedSms.setRoamingStatus(mAIMSICDService.getCellTracker().getDevice().isRoaming());
        capturedSms.setLat(mAIMSICDService.getCell().getLat());
        capturedSms.setLon(mAIMSICDService.getCell().getLon());

        // Only alert if the timestamp is not in the data base
        if(!mDbAdapter.isTimeStampInDB(logcat_timestamp)) {
            mDbAdapter.storeCapturedSms(capturedSms);
            mDbAdapter.toEventLog(3, "Detected Type-0 SMS");
            startPopUpInfo(mContext, 6);
        } else {
            Log.w(TAG, "Detected Sms already logged");
        }

    }

    private void parseMwiSms(String logcatLine, Long logcat_timestamp) {

        CapturedSmsData capturedSms = new CapturedSmsData();
        String smsText = findSmsText(logcatLine);
        String num = findSmsNumber(logcatLine);

        capturedSms.setSenderNumber(num == null ? "null" : num);
        capturedSms.setSenderMsg(smsText == null ? "null" : smsText);
        capturedSms.setSmsTimestamp(logcat_timestamp);
        capturedSms.setSmsType("MWI");
        capturedSms.setLAC(mAIMSICDService.getCellTracker().getCell().getLAC());
        capturedSms.setCellId(mAIMSICDService.getCellTracker().getCell().getCellId());
        capturedSms.setTechnology(Helpers.getNetworkTypeName(mAIMSICDService.getCell().getNetType()));
        capturedSms.setRoamingStatus(mAIMSICDService.getCellTracker().getDevice().isRoaming());
        capturedSms.setLat(mAIMSICDService.getCell().getLat());
        capturedSms.setLon(mAIMSICDService.getCell().getLon());

        //only alert if timestamp is not in the data base
        if(!mDbAdapter.isTimeStampInDB(logcat_timestamp)) {
            mDbAdapter.storeCapturedSms(capturedSms);
            mDbAdapter.toEventLog(4, "Detected MWI SMS");
            startPopUpInfo(mContext, 7);
        } else {
            Log.w(TAG, " Detected Sms already logged");
        }
    }

    private String findSmsText(String line) {
        if(line.contains("SMS message body (raw):") && line.contains("'")) {
            return line.substring(line.indexOf("'") + 1,
                                  line.length() - 1);
        }
        return null;
    }

    private String findSmsNumber(String line) {
        if(line.contains("SMS originating address:") && line.contains("+")) {
            return line.substring(line.indexOf("+"));
        } else if(line.contains("OrigAddr")) {
            return line.substring(line.indexOf("OrigAddr")).replace("OrigAddr", "").trim();
        }
        return null;
    }

    private void parseWapPushSms(Queue logcatLine, /*String[] postWapMessageLines,*/ Long logcat_timestamp) {
        CapturedSmsData capturedSms = new CapturedSmsData();

        List<Object> lines = Arrays.asList(logcatLine.toArray());
        String smsText = null;
        String smsNum = null;
        String tmpText, tmpNum;
        for(Object line : lines) {
            tmpText = findSmsText(line.toString());
            tmpNum = findSmsNumber(line.toString());
            if(tmpText != null) {
                smsText = tmpText;
            }
            if(tmpNum != null) {
                smsNum = tmpNum;
            }
        }

        capturedSms.setSenderNumber(smsNum == null ? "null" : smsNum);
        capturedSms.setSenderMsg(smsText == null ? "null" : smsText);
        capturedSms.setSmsTimestamp(logcat_timestamp);
        capturedSms.setSmsType("WAPPUSH");
        capturedSms.setLAC(mAIMSICDService.getCellTracker().getCell().getLAC());
        capturedSms.setCellId(mAIMSICDService.getCellTracker().getCell().getCellId());
        capturedSms.setTechnology(Helpers.getNetworkTypeName(mAIMSICDService.getCell().getNetType()));
        capturedSms.setRoamingStatus(mAIMSICDService.getCellTracker().getDevice().isRoaming());
        capturedSms.setLat(mAIMSICDService.getCell().getLat());
        capturedSms.setLon(mAIMSICDService.getCell().getLon());

        //only alert if timestamp is not in the data base
        if(!mDbAdapter.isTimeStampInDB(logcat_timestamp)) {
            mDbAdapter.storeCapturedSms(capturedSms);
            mDbAdapter.toEventLog(6, "Detected WAPPUSH SMS");
            startPopUpInfo(mContext, 8);
        } else {
            Log.w(TAG, "Detected SMS already logged");
        }
    }

    private void startPopUpInfo(Context context, int mode) {
        Intent i = new Intent(context, CustomPopUp.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("display_mode", mode);
        context.startActivity(i);
    }

    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mAIMSICDService = ((AimsicdService.AimscidBinder) service).getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d(TAG, "Disconnected SMS Detection Service");
            mBound = false;
        }
    };
}
