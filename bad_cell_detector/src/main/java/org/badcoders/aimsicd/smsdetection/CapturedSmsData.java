package org.badcoders.aimsicd.smsdetection;

public class CapturedSmsData {

    private long id;
    private String senderNumber;
    private String senderMsg;
    private long smsTimestamp;
    private String smsType;
    private int lac;
    private int cellId;
    private String technology;
    private boolean isRoaming;
    private double lat;
    private double lon;

    public CapturedSmsData() {

    }
    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String current_nettype) {
        this.technology = current_nettype;
    }

    public boolean getRoamingStatus() {
        return isRoaming;
    }

    public void setRoamingStatus(boolean current_roam_status) {
        this.isRoaming = current_roam_status;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double current_gps_lat) {
        this.lat = current_gps_lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double current_gps_lon) {
        this.lon = current_gps_lon;
    }

    public int getLAC() {
        return lac;
    }

    public void setLAC(int current_lac) {
        this.lac = current_lac;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int current_cid) {
        this.cellId = current_cid;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getSenderMsg() {
        return senderMsg;
    }

    public void setSenderMsg(String senderMsg) {
        this.senderMsg = senderMsg;
    }

    public Long getSmsTimestamp() {
        return smsTimestamp;
    }

    public void setSmsTimestamp(Long smsTimestamp) {
        this.smsTimestamp = smsTimestamp;
    }

    public String getSmsType() {
        return smsType;
    }

    public void setSmsType(String smsType) {
        this.smsType = smsType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
