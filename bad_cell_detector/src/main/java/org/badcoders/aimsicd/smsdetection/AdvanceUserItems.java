package org.badcoders.aimsicd.smsdetection;

public class AdvanceUserItems {

    private String detection_string;
    private String detection_type;

    public String getDetectionString() {
        return detection_string;
    }

    public void setDetectionString(String detection_string) {
        this.detection_string = detection_string;
    }

    public String getDetectionType() {
        return detection_type;
    }

    public void setDetectionType(String detection_type) {
        this.detection_type = detection_type;
    }

}
