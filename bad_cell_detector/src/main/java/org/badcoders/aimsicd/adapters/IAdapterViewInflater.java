package org.badcoders.aimsicd.adapters;

import android.view.View;
import android.view.ViewGroup;

public interface IAdapterViewInflater<T> {
    View inflate(BaseInflaterAdapter<T> adapter, int pos, View convertView, ViewGroup parent);
}
