package org.badcoders.aimsicd.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class BaseInflaterAdapter<T> extends BaseAdapter {

    private final List<T> items = new ArrayList<>();

    private IAdapterViewInflater<T> adapterViewInflater;

    public BaseInflaterAdapter(IAdapterViewInflater<T> viewInflater) {
        adapterViewInflater = viewInflater;
    }

    public BaseInflaterAdapter(List<T> items, IAdapterViewInflater<T> viewInflater) {
        this.items.addAll(items);
        adapterViewInflater = viewInflater;
    }

    public void addItem(T item) {
        items.add(item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int pos) {
        return getTItem(pos);
    }

    public T getTItem(int pos) {
        return items.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        return adapterViewInflater != null ? adapterViewInflater.inflate(this, pos, convertView, parent)
                       : null;
    }
}
