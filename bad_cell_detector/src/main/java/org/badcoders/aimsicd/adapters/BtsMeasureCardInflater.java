package org.badcoders.aimsicd.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.Formatter;

public class BtsMeasureCardInflater implements IAdapterViewInflater<Cell> {

    @Override
    public View inflate(final BaseInflaterAdapter<Cell> adapter,
                        final int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.bts_measure_data, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Cell item = adapter.getTItem(pos);
        holder.updateDisplay(item, pos);

        return convertView;
    }

    private class ViewHolder {

        private final View mRootView;

        private TextView cellid;
        private TextView lac;
        private TextView technology;
        private TextView time;
        private TextView lat;
        private TextView lon;
        private TextView accuracy;
        private TextView dbm;
        private TextView speed;

        private TextView mRecordId;

        public ViewHolder(View rootView) {
            mRootView = rootView;

            cellid = (TextView) mRootView.findViewById(R.id.cellid);
            lac = (TextView) mRootView.findViewById(R.id.lac);
            technology = (TextView) mRootView.findViewById(R.id.technology);
            time = (TextView) mRootView.findViewById(R.id.last_seen);
            lat = (TextView) mRootView.findViewById(R.id.lat);
            lon = (TextView) mRootView.findViewById(R.id.lon);
            dbm = (TextView) mRootView.findViewById(R.id.signal);
            accuracy = (TextView) mRootView.findViewById(R.id.accuracy);
            speed = (TextView) mRootView.findViewById(R.id.speed);

            mRecordId = (TextView) mRootView.findViewById(R.id.record_id);
            rootView.setTag(this);
        }

        public void updateDisplay(Cell item, int counter) {
            cellid.setText(String.valueOf(item.getCellId()));
            lac.setText(String.valueOf(item.getLAC()));
            time.setText(Formatter.formatDateTime(item.getLastSeen()));
            lat.setText(Formatter.formatLocation(item.getLat()));
            lon.setText(Formatter.formatLocation(item.getLon()));
            dbm.setText(String.valueOf(item.getDBM()));
            technology.setText(item.getTechnology());
            accuracy.setText(Formatter.formatLocation(item.getAccuracy()));
            speed.setText(Formatter.formatLocation(item.getSpeed()));

            mRecordId.setText(String.valueOf(counter));
        }
    }
}
