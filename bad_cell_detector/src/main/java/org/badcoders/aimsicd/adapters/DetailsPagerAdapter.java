package org.badcoders.aimsicd.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.fragments.CellCompareFragment;
import org.badcoders.aimsicd.fragments.DbViewerFragment;
import org.badcoders.aimsicd.fragments.DeviceFragment;
import org.badcoders.aimsicd.fragments.MapViewFragment;
import org.badcoders.aimsicd.fragments.NeighboringCells;

/**
 * Adapter to allow swiping between various detail fragments
 */
public class DetailsPagerAdapter extends FragmentPagerAdapter {

    private static DeviceFragment deviceFragment;
    private static NeighboringCells neighboringCells;
    private static CellCompareFragment cellCompareFragment;
    private static DbViewerFragment dbViewerFragment;
    private static MapViewFragment mapViewFragment;

    public enum FragmentLocation {
        DEVICE_FRAGMENT(0, R.string.device_info),
        NEIGHBORING_CELLS(1, R.string.neighboring_cells_title),
        CELL_COMPARE_FRAGMENT(2, R.string.verify_cell),
        DB_VIEWER_FRAGMENT(3, R.string.db_viewer),
        MAP_VIEW_FRAGMENT(4, R.string.map_view);

        public final int position;

        public final int titleResource;

        FragmentLocation(int position, int titleResource) {
            this.position = position;
            this.titleResource = titleResource;
        }
    }

    private Context context;

    public DetailsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        if(position == FragmentLocation.DEVICE_FRAGMENT.position)
            return deviceFragment == null ? deviceFragment = new DeviceFragment() : deviceFragment;

        else if(position == FragmentLocation.NEIGHBORING_CELLS.position)
            return neighboringCells == null ? neighboringCells = new NeighboringCells() : neighboringCells;

        else if(position == FragmentLocation.CELL_COMPARE_FRAGMENT.position)
            return cellCompareFragment == null ? cellCompareFragment = new CellCompareFragment() : cellCompareFragment;

        else if(position == FragmentLocation.DB_VIEWER_FRAGMENT.position)
            return dbViewerFragment == null ? dbViewerFragment = new DbViewerFragment() : dbViewerFragment;

        else if(position == FragmentLocation.MAP_VIEW_FRAGMENT.position)
            return mapViewFragment == null ? mapViewFragment = new MapViewFragment() : mapViewFragment;

        return new DeviceFragment();
    }

//    @Override
//    public long getItemId(int position) {
//        // map position to position in aimsicd.getNavDrawerConfiguration
//        switch(position) {
//            case 0:
//                return 4;
//            case 1:
//                return 5;
//            case 2:
//                return 7;
//        }
//
//        return -1;
//    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if(position == FragmentLocation.DEVICE_FRAGMENT.position)
            return context.getString(FragmentLocation.DEVICE_FRAGMENT.titleResource);

        else if(position == FragmentLocation.NEIGHBORING_CELLS.position)
            return context.getString(FragmentLocation.NEIGHBORING_CELLS.titleResource);

        else if(position == FragmentLocation.CELL_COMPARE_FRAGMENT.position)
            return context.getString(FragmentLocation.CELL_COMPARE_FRAGMENT.titleResource);

        else if(position == FragmentLocation.DB_VIEWER_FRAGMENT.position)
            return context.getString(FragmentLocation.DB_VIEWER_FRAGMENT.titleResource);

        else if(position == FragmentLocation.MAP_VIEW_FRAGMENT.position)
            return context.getString(FragmentLocation.MAP_VIEW_FRAGMENT.titleResource);
        else
            return "";
    }

}
