package org.badcoders.aimsicd.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.Formatter;

/**
 * Contains the data and definitions of all the items of the XML layout
 */
public class DbeImportCardInflater implements IAdapterViewInflater<Cell> {

    @Override
    public View inflate(final BaseInflaterAdapter<Cell> adapter,
                        final int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.dbe_import_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Cell item = adapter.getTItem(pos);
        holder.updateDisplay(item, pos);
        return convertView;
    }

    private class ViewHolder {

        private final View mRootView;

        private final TextView source;
        private final TextView rat;
        private final TextView mcc;
        private final TextView mnc;
        private final TextView lac;
        private final TextView cellId;
        private final TextView pcs;
        private final TextView lat;
        private final TextView lon;
        private final TextView averageSignal;
        private final TextView samples;
        private final TextView firstSeen;
        private final TextView lastSeen;
        private final TextView record;

        public ViewHolder(View rootView) {
            mRootView = rootView;
            cellId = (TextView) mRootView.findViewById(R.id.cell_id);
            mcc = (TextView) mRootView.findViewById(R.id.mcc);
            lac = (TextView) mRootView.findViewById(R.id.lac);
            mnc = (TextView) mRootView.findViewById(R.id.mnc);
            lat = (TextView) mRootView.findViewById(R.id.latitude);
            lon = (TextView) mRootView.findViewById(R.id.longitude);
            pcs = (TextView) mRootView.findViewById(R.id.psc);
            rat = (TextView) mRootView.findViewById(R.id.rat);
            samples = (TextView) mRootView.findViewById(R.id.samples);
            averageSignal = (TextView) mRootView.findViewById(R.id.average_signal);
            firstSeen = (TextView) mRootView.findViewById(R.id.first_seen);
            lastSeen = (TextView) mRootView.findViewById(R.id.last_seen);
            source = (TextView) mRootView.findViewById(R.id.source);

            record = (TextView) mRootView.findViewById(R.id.record_id);
            rootView.setTag(this);
        }

        public void updateDisplay(Cell item, int counter) {
            cellId.setText(String.valueOf(item.getCellId()));
            lac.setText(String.valueOf(item.getLAC()));
            pcs.setText(String.valueOf(item.getPSC()));
            rat.setText(item.getTechnology());
            mcc.setText(String.valueOf(item.getMCC()));
            mnc.setText(String.valueOf(item.getMNC()));
            lat.setText(Formatter.formatLocation(item.getLat()));
            lon.setText(Formatter.formatLocation(item.getLon()));
            averageSignal.setText(String.valueOf(item.getDBM()));
            samples.setText(String.valueOf(item.getSamples()));
            firstSeen.setText(Formatter.formatDate(item.getFirstSeen()));
            lastSeen.setText(Formatter.formatDate(item.getLastSeen()));
            source.setText(item.getSource());
            record.setText(String.valueOf(counter));
        }
    }
}
