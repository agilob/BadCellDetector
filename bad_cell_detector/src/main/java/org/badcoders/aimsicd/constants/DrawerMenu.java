package org.badcoders.aimsicd.constants;

import org.badcoders.aimsicd.drawer.NavDrawerItem;

/**
 * Constants for Menu
 */
public class DrawerMenu {

    /**
     * Constants of id for NavDrawerItem
     * <p>Relates to {@link NavDrawerItem#getId()}<br />
     */
    public static class ID {

        /**
         * Constants of section of menu
         */
        public static final int SECTION_MAIN = 10;
        public static final int SECTION_TRACKING = 20;
        public static final int SECTION_SETTINGS = 30;
        public static final int SECTION_APPLICATION = 40;

        /**
         * Constants of item of 'main' section of menu
         */
        public static class MAIN {
            public static final int NEIGHBORING_CELLS = 110;
            public static final int PHONE_SIM_DETAILS = 100;
            public static final int VERIFY_CURRENT_CELL = 215;
            public static final int DB_VIEWER = 130;
            public static final int ANTENNA_MAP_VIEW = 140;
            public static final int AT_COMMAND_INTERFACE = 150;
        }

        /**
         * Constants of item of 'tracking' section of menu
         */
        public static class TRACKING {
            public static final int TOGGLE_ATTACK_DETECTION = 200; //Toggle Attack Detection
            public static final int TOGGLE_CELL_TRACKING = 210; //Toggle Cell Tracking
            public static final int TRACK_FEMTOCELL = 220; //Track Femtocell
        }

        /**
         * Constants of item of 'settings' section of menu
         */
        public static class SETTINGS {
            public static final int PREFERENCES = 300;
            public static final int BACKUP_DB = 310; //Backup DataBase
            public static final int RESTORE_DB = 320; //Restore DataBase
            public static final int RESET_DB = 330; //Reset DataBase
        }

        /**
         * Constants of item of 'application' section of menu
         */
        public static class APPLICATION {
            public static final int DOWNLOAD_LOCAL_BTS_DATA = 400; //Download Local BST Data
            public static final int UPLOAD_LOCAL_BTS_DATA = 410; //Upload Local BST Data
            public static final int ABOUT = 430; //About Aimsicd
            public static final int QUIT = 460; //Quit
        }

    }

    /**
     * The constant indicates the number of types of menu items
     */
    public final static int COUNT_OF_MENU_TYPE = 2;

}
