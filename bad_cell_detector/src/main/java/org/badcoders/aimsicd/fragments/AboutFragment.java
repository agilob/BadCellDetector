package org.badcoders.aimsicd.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.activities.CustomPopUp;

public class AboutFragment extends Fragment {

    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.about_fragment, container, false);
        String version;

        PackageManager manager = mContext.getPackageManager();
        try {
            PackageInfo info = manager != null ? manager.getPackageInfo(mContext.getPackageName(),
                                                                         0) : null;
            version = (info != null ? info.versionName : "");
        } catch(PackageManager.NameNotFoundException e) {
            version = "Unknown";
        }

        TextView versionNumber;
        if(v != null) {
            versionNumber = (TextView) v.findViewById(R.id.aimsicd_version);
            versionNumber.setText(getString(R.string.app_version) + version);

            View imgView_idle = v.findViewById(R.id.imgView_idle);
            imgView_idle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startPopUpInfo(mContext, 0);
                }
            });

            View imgView_normal = v.findViewById(R.id.imgView_normal);
            imgView_normal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startPopUpInfo(mContext, 1);
                }
            });

            View imgView_medium = v.findViewById(R.id.imgView_medium);
            imgView_medium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startPopUpInfo(mContext, 2);
                }
            });

            View imgView_high = v.findViewById(R.id.imgView_high);
            imgView_high.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startPopUpInfo(mContext, 3);
                }
            });

            View imgView_danger = v.findViewById(R.id.imgView_danger);
            imgView_danger.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startPopUpInfo(mContext, 4);
                }
            });

            View imgView_run = v.findViewById(R.id.imgView_run);
            imgView_run.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startPopUpInfo(mContext, 5);
                }
            });

            View dependencies = v.findViewById(R.id.dependencies);
            dependencies.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   startPopUpInfo(mContext, 9);
               }
           });
        }
        return v;
    }

    @Override
    public void onAttach(Context mContext) {
        super.onAttach(mContext);
        this.mContext = mContext;
    }

    private void startPopUpInfo(Context context, int mode) {
        Intent i = new Intent(context, CustomPopUp.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("display_mode", mode);
        context.startActivity(i);
    }
}
