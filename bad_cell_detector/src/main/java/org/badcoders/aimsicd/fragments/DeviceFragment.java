package org.badcoders.aimsicd.fragments;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Device;
import org.badcoders.aimsicd.service.AimsicdService;
import org.badcoders.aimsicd.utils.Helpers;

public class DeviceFragment extends Fragment {

    private static final String TAG = "DeviceFragment";
    private boolean mBound;

    private AimsicdService mAimsicdService;
    private Context mContext;
    private TelephonyManager tm;
    private View mView;

    @Override
    public void onAttach(Context mContext) {
        super.onAttach(mContext);
        this.mContext = mContext;
        tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        // Bind to LocalService
        Intent intent = new Intent(this.mContext, AimsicdService.class);

        this.mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.device, container, false);

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!mBound) {
            // Bind to LocalService
            Intent intent = new Intent(mContext, AimsicdService.class);
            mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }

        mContext.registerReceiver(mMessageReceiver,
                                  new IntentFilter(AimsicdService.UPDATE_DISPLAY));
        updateUI();
    }

    @Override
    public void onPause() {
        super.onPause();
        mContext.unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unbind from the service
        if(mBound) {
            mContext.unbindService(mConnection);
            mBound = false;
        }
    }


    /**
     * Service Connection to bind the activity to the service
     */
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mAimsicdService = ((AimsicdService.AimscidBinder) service).getService();
            mBound = true;
            updateUI();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            if(bundle != null && bundle.getBoolean("update")) {
                Toast.makeText(mContext, context.getString(R.string.refreshing_display),
                               Toast.LENGTH_SHORT).show();
                updateUI();
            }
        }
    };

    private void updateUI() {
        if(isAdded() == false)
            return;
        TextView content;
        if(mBound) {
            mAimsicdService.getCellTracker().refreshDevice();
            Device mDevice = mAimsicdService.getCellTracker().getDevice();
            switch(mDevice.getPhoneID()) {
                case TelephonyManager.PHONE_TYPE_GSM: {
                    content = (TextView) mView.findViewById(R.id.network_lac);
                    content.setText(String.valueOf(mAimsicdService.getCell().getLAC()));
                    content = (TextView) mView.findViewById(R.id.network_cellid);
                    content.setText(String.valueOf(mAimsicdService.getCell().getCellId()));
                    break;
                }

                case TelephonyManager.PHONE_TYPE_CDMA: {
                    content = (TextView) mView.findViewById(R.id.cell_id);
                    content.setText(String.valueOf(mAimsicdService.getCell().getCellId()));

                    content = (TextView) mView.findViewById(R.id.LAC);
                    content.setText(String.valueOf(mAimsicdService.getCell().getLAC()));

                    content = (TextView) mView.findViewById(R.id.system_id);
                    content.setText(String.valueOf(mAimsicdService.getCell().getMNC()));
                    break;
                }
            }

            if(mAimsicdService.getCell().getPSC() != -1) {
                content = (TextView) mView.findViewById(R.id.network_psc);
                content.setText(String.valueOf(mAimsicdService.getCell().getPSC()));
            }

            content = (TextView) mView.findViewById(R.id.sim_country);
            content.setText(mDevice.getSimCountry());
            content = (TextView) mView.findViewById(R.id.sim_operator_id);
            content.setText(mDevice.getSimOperator());
            content = (TextView) mView.findViewById(R.id.sim_operator_name);
            content.setText(mDevice.getSimOperatorName());
            content = (TextView) mView.findViewById(R.id.sim_imsi);
            content.setText(mDevice.getSimSubs());
            content = (TextView) mView.findViewById(R.id.sim_serial);
            content.setText(mDevice.getSimSerial());

            content = (TextView) mView.findViewById(R.id.device_type);
            content.setText(mDevice.getPhoneType());
            content = (TextView) mView.findViewById(R.id.device_imei);
            content.setText(mDevice.getIMEI());
            content = (TextView) mView.findViewById(R.id.device_version);
            content.setText(mDevice.getIMEIv());
            content = (TextView) mView.findViewById(R.id.network_name);
            content.setText(mDevice.getNetworkOperatorName());
            content = (TextView) mView.findViewById(R.id.network_code);
            content.setText(mDevice.getOperatorCode());
            content = (TextView) mView.findViewById(R.id.network_type);
            content.setText(Helpers.getNetworkTypeName(tm.getNetworkType()));

            content = (TextView) mView.findViewById(R.id.data_activity);
            content.setText(mDevice.getDataActivity());
            content = (TextView) mView.findViewById(R.id.network_roaming);
            content.setText(mDevice.isRoaming() == true ?
                                    getString(R.string.yes) : getString(R.string.no));
        }
    }
}
