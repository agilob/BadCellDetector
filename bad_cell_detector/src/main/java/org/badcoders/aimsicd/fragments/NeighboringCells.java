package org.badcoders.aimsicd.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.BaseInflaterAdapter;
import org.badcoders.aimsicd.adapters.NeighbourCellCardInflater;
import org.badcoders.aimsicd.rilexecutor.RilExecutor;
import org.badcoders.aimsicd.service.AimsicdService;
import org.badcoders.aimsicd.service.CellTracker;
import org.badcoders.aimsicd.model.Cell;

import java.util.List;

/**
 * This was introduced by Aussie in the Pull Request Commit:
 * https://github.com/xLaMbChOpSx/Android-IMSI-Catcher-Detector/commit/6d8719ab356a3ecbd0b526a9ded0cabb17ab2021
 *
 * Where he writes:
 * ""
 * Advanced Cell Fragment added to display the Neighbouring Cell information in two ways firstly
 * through telephony manager methods which does not work on Samsung Devices, a fallback is available
 * through the methods developed by Alexey and will display if these are successful.
 * Ciphering Indicator also uses Alexey's methods and will display on Samsung devices.
 *
 * Description:    This class updates the CellInfo fragment. This is also known as
 * the Neighboring Cells info, which is using the MultiRilClient to
 * show neighboring cells on the older Samsung Galaxy S2/3 series.
 * It's refresh rate is controlled in the settings by:* arrays.xml:
 * pref_refresh_entries    (the names)
 * pref_refresh_values     (the values in seconds)
 *
 * Dependencies:   Seem that this is intimately connected to: CellTracker.java service...
 * TODO:   1)  Use an IF check, in order not to run the MultiRilClient on non supported devices
 * as this will cause excessive logcat spam.
 * TODO:   2) Might wanna make the refresh rate lower/higher depending on support
 */
public class NeighboringCells extends Fragment {

    private static final String TAG = "NeighboringCells";

    public static final int STOCK_REQUEST = 1;
    public static final int SAMSUNG_MULTIRIL_REQUEST = 2;

    private AimsicdService mAimsicdService;
    private RilExecutor rilExecutor;
    private Context mContext;
    private final Handler timerHandler = new Handler();

    private boolean mBound;

    private List<Cell> neighboringCells;

    //Layout items
    private ListView lv;
    private TextView mCipheringIndicator;

    private final Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            updateUI();
            timerHandler.postDelayed(this, CellTracker.REFRESH_RATE);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context mContext) {
        super.onAttach(mContext);

        this.mContext = mContext;
        Intent intent = new Intent(mContext, AimsicdService.class);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.refresh_menu_item, menu);
    }

    @Override
    public void onPause() {
        super.onPause();

        timerHandler.removeCallbacks(timerRunnable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.button_refresh:
                updateUI();
                return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mBound == false) {
            // Bind to LocalService
            Intent intent = new Intent(mContext, AimsicdService.class);
            mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            mBound = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.cell_fragment, container, false);

        if(view != null) {
            lv = (ListView) view.findViewById(R.id.list_of_cells);
            mCipheringIndicator = (TextView) view.findViewById(R.id.ciphering_indicator);
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unbind from the service
        if(mBound) {
            mContext.unbindService(mConnection);
            mBound = false;
        }
        timerHandler.removeCallbacks(timerRunnable);
    }

    /**
     * Service Connection to bind the activity to the service
     */
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mAimsicdService = ((AimsicdService.AimscidBinder) service).getService();
            rilExecutor = mAimsicdService.getRilExecutor();
            mBound = true;
            updateUI();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            updateUI();
        }
    }

    private void updateUI() {
        if(mBound && rilExecutor.mMultiRilCompatible) {
            rilExecutor.getRilExecutorStatus();
            new CellAsyncTask().execute(SAMSUNG_MULTIRIL_REQUEST);
        } else {
            new CellAsyncTask().execute(STOCK_REQUEST);
        }
    }

    void updateCipheringIndicator() {
        final List<String> list = rilExecutor.getCipheringInfo();
        if(list != null) {
            mCipheringIndicator.setText(TextUtils.join("\n", list));
        }
    }

    boolean getStockNeighbouringCells() {
        if(mBound) {
            neighboringCells = mAimsicdService.getCellTracker().updateNeighbouringCells();
            return !neighboringCells.isEmpty();
        }

        return false;
    }

    void updateStockNeighbouringCells() {
        if(!neighboringCells.isEmpty()) {
            BaseInflaterAdapter<Cell> adapter = new BaseInflaterAdapter<>(new NeighbourCellCardInflater());
            for(Cell cell : neighboringCells) {
                adapter.addItem(cell);
            }

            lv.setAdapter(adapter);
        }
    }

    void updateNeighbouringCells() {
        rilExecutor.getNeighbours();
    }

    void getSamSungMultiRil() {
        if(mBound && rilExecutor.mMultiRilCompatible) {
            new CellAsyncTask().execute(SAMSUNG_MULTIRIL_REQUEST);
        }
    }

    private class CellAsyncTask extends AsyncTask<Integer, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... type) {
            switch(type[0]) {
                case STOCK_REQUEST:
                    Log.w(TAG, "Using stock request to get neighbouring cells");
                    return getStockNeighbouringCells();
                case SAMSUNG_MULTIRIL_REQUEST:
                    if(mBound) {
                        Log.w(TAG, "Using samsung request to get neighbouring cells");
                        updateNeighbouringCells();
                        updateCipheringIndicator();
                    }
                    break;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result) {
                updateStockNeighbouringCells();
            } else {
                getSamSungMultiRil();
            }
        }
    }

}
