package org.badcoders.aimsicd.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.BaseInflaterAdapter;
import org.badcoders.aimsicd.adapters.BtsMeasureCardInflater;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.adapters.DbViewerSpinnerAdapter;
import org.badcoders.aimsicd.adapters.DbeImportCardInflater;
import org.badcoders.aimsicd.adapters.EventLogCardInflater;
import org.badcoders.aimsicd.adapters.EventLogItemData;
import org.badcoders.aimsicd.adapters.UniqueBtsCardInflater;
import org.badcoders.aimsicd.constants.DBTableColumnIds;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.smsdetection.CapturedSmsCardInflater;
import org.badcoders.aimsicd.smsdetection.CapturedSmsData;
import org.badcoders.aimsicd.smsdetection.DetectionStringsCardInflater;
import org.badcoders.aimsicd.smsdetection.DetectionStringsData;
import org.badcoders.aimsicd.utils.Formatter;

/**
 * Class that handles the display of the items in the 'Database Viewer' (DBV)
 */
public class DbViewerFragment extends Fragment {

    private static final int UNIQUE_CELLS = 0;
    private static final int MEASURES = 1;
    private static final int IMPORTED_CELLS = 2;
    private static final int SILENT_SMS = 3;
    private static final int EVENT_LOG = 4;
    private static final int DETECTION_STRINGS = 5;

    private DatabaseAdapter databaseAdapter;

    private Spinner tblSpinner;
    private ListView lv;
    private View emptyView;

    private int menuSelected = UNIQUE_CELLS;

    public DbViewerFragment() {
    }

    @Override
    public void onAttach(Context mContext) {
        super.onAttach(mContext);
        databaseAdapter = new DatabaseAdapter(mContext);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.refresh_menu_item, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.db_view, container, false);

        lv = (ListView) view.findViewById(R.id.list_view);
        emptyView = view.findViewById(R.id.db_list_empty);
        tblSpinner = (Spinner) view.findViewById(R.id.table_spinner);
        tblSpinner.setAdapter(new DbViewerSpinnerAdapter(getActivity(),
                                                         R.layout.item_spinner_db_viewer));
        tblSpinner.setOnItemSelectedListener(new MySelectorListener());

        return view;
    }

    private class MySelectorListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, final int position, long id) {

            new AsyncTask<Void, Void, BaseInflaterAdapter>() {

                @Override
                protected BaseInflaterAdapter doInBackground(Void... params) {
                    menuSelected = position;
                    return setupAdapter(position);
                }

                @Override
                protected void onPostExecute(BaseInflaterAdapter adapter) {
                    if(getActivity() == null)
                        return; // fragment detached

                    if(adapter != null) {
                        lv.setAdapter(adapter);
                        lv.setVisibility(View.VISIBLE);
                        emptyView.setVisibility(View.GONE);
                    } else {
                        lv.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    }
                    getActivity().setProgressBarIndeterminateVisibility(false);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }

    }

    private BaseInflaterAdapter setupAdapter(int position) {
        Cursor result = null;

        switch(position) {
            case UNIQUE_CELLS:
                result = databaseAdapter.returnObservedCursor();
                break;

            case MEASURES:
                result = databaseAdapter.returnMeasuresCursor();
                break;

            case IMPORTED_CELLS:
                result = databaseAdapter.returnImportedCursor();
                break;

            case SILENT_SMS:
                result = databaseAdapter.returnSmsData();
                break;

            case EVENT_LOG:
                result = databaseAdapter.returnEventLogData();
                break;

            case DETECTION_STRINGS:
                result = databaseAdapter.returnDetectionStrings();
                break;

            //case 7: // DETECTION_FLAGS:       ("DetectionFlags")
            //    result = databaseAdapter.getDetectionFlagsData();
            //    break;

            default:
                result = null;
        }

        BaseInflaterAdapter adapter = null;
        if(result != null) {
            adapter = buildTable(result);
            result.close();
        }
        return adapter;
    }

    private BaseInflaterAdapter buildTable(Cursor cursor) {

        if(cursor != null && cursor.getCount() > 0) {

            switch(menuSelected) {
                case UNIQUE_CELLS: {
                    BaseInflaterAdapter<Cell> adapter = new BaseInflaterAdapter<>(new UniqueBtsCardInflater());
                    while(cursor.moveToNext()) {
                        adapter.addItem(DatabaseAdapter.observedToCell(cursor));
                    }
                    if(!cursor.isClosed()) {
                        cursor.close();
                    }
                    return adapter;
                }

                case MEASURES: {
                    BaseInflaterAdapter<Cell> adapter = new BaseInflaterAdapter<>(new BtsMeasureCardInflater());
                    while(cursor.moveToNext()) {
                        adapter.addItem(DatabaseAdapter.measureCursorToCell(cursor));
                    }
                    if(!cursor.isClosed()) {
                        cursor.close();
                    }
                    return adapter;
                }

                case IMPORTED_CELLS: {
                    BaseInflaterAdapter<Cell> adapter = new BaseInflaterAdapter<>(new DbeImportCardInflater());
                    while(cursor.moveToNext()) {
                        adapter.addItem(DatabaseAdapter.importedCursorToCell(cursor));
                    }
                    if(!cursor.isClosed()) {
                        cursor.close();
                    }
                    return adapter;
                }

                case SILENT_SMS: {
                    BaseInflaterAdapter<CapturedSmsData> adapter
                            = new BaseInflaterAdapter<>(new CapturedSmsCardInflater());
                    while(cursor.moveToNext()) {
                        CapturedSmsData getdata = new CapturedSmsData();
                        // TODO: Add class and smsc
                        // TODO: Check order as in DB schema (ER diagram)
                        getdata.setSmsTimestamp(cursor.getLong(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_TIMESTAMP)));
                        getdata.setSmsType(cursor.getString(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_SMS_TYPE)));
                        getdata.setSenderNumber(cursor.getString(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_SENDER_NUMBER)));
                        //getdata.setSenderNumber(tableData.getString(tableData.getColumnIndex(DBTableColumnIds.SMS_DATA_SMSC)));
                        getdata.setSenderMsg(cursor.getString(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_SENDER_MSG)));
                        //getdata.setSenderNumber(tableData.getString(tableData.getColumnIndex(DBTableColumnIds.SMS_DATA_CLASS)));
                        getdata.setLAC(cursor.getInt(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_LAC)));
                        getdata.setCellId(cursor.getInt(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_CID)));
                        getdata.setTechnology(cursor.getString(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_RAT)));
                        getdata.setRoamingStatus(cursor.getInt(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_ROAM_STATE)) > 0);
                        getdata.setLat(cursor.getDouble(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_GPS_LAT)));
                        getdata.setLon(cursor.getDouble(
                                cursor.getColumnIndex(DBTableColumnIds.SMS_DATA_GPS_LON)));
                        adapter.addItem(getdata);
                    }
                    if(!cursor.isClosed()) {
                        cursor.close();
                    }
                    return adapter;
                }

                case EVENT_LOG: {
                    BaseInflaterAdapter<EventLogItemData> adapter
                            = new BaseInflaterAdapter<>(new EventLogCardInflater());

                    while(cursor.moveToNext()) {
                        EventLogItemData data = new EventLogItemData( //@formatter:off
                            cursor.getLong(cursor.getColumnIndex("time")),
                            String.valueOf(cursor.getInt(cursor.getColumnIndex("LAC"))),
                            String.valueOf(cursor.getInt(cursor.getColumnIndex("CID"))),
                            String.valueOf(cursor.getInt(cursor.getColumnIndex("PSC"))),
                            Formatter.formatLocation(cursor.getDouble(cursor.getColumnIndex("gpsd_lat"))),
                            Formatter.formatLocation(cursor.getDouble(cursor.getColumnIndex("gpsd_lon"))),
                            String.valueOf(cursor.getInt(cursor.getColumnIndex("gpsd_accu"))),
                            String.valueOf(cursor.getInt(cursor.getColumnIndex("DF_id"))),
                            String.valueOf(cursor.getString(cursor.getColumnIndex("DF_description"))),
                            String.valueOf((cursor.getPosition() + 1) + " / " + cursor.getCount()
                            )); //@formatter:on
                        adapter.addItem(data);
                    }
                    if(!cursor.isClosed()) {
                        cursor.close();
                    }
                    return adapter;
                }

                case DETECTION_STRINGS: {
                    // Storage of Abnormal SMS detection strings
                    BaseInflaterAdapter<DetectionStringsData> adapter
                            = new BaseInflaterAdapter<>(new DetectionStringsCardInflater());
                    while(cursor.moveToNext()) {
                        adapter.addItem(DatabaseAdapter.detectionStringToDetectionStringsData(cursor));
                    }
                    if(!cursor.isClosed()) {
                        cursor.close();
                    }
                    return adapter;
                }
            }
        } else {
            return null;
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.button_refresh:
                lv.setAdapter(setupAdapter(menuSelected));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
