package org.badcoders.aimsicd;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import org.badcoders.aimsicd.utils.TinyDB;

import java.lang.ref.WeakReference;

//@ReportsCrashes(
//        formUri = "https://www.agilob.net/projects/aimsicd/reporter.php",
//        httpMethod = HttpSender.Method.POST
//)
public class MyApplication extends Application {

    public static final String NOTIFICATION_STOP = "com.SecUpwN.MyApplication.stop";
    public static final String TRACKING_CHANGE = "com.SecUpwN.MyApplication.trackingChange";

    private WeakReference<Aimsicd> mMainActivity = new WeakReference<>(null);

    private ServiceBroadcastReceiver mReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        TinyDB.getInstance().init(getApplicationContext());

        mReceiver = new ServiceBroadcastReceiver();
        mReceiver.register();
//        ACRA.init(this);
    }

    public void setMainActivity(Aimsicd mainActivity) {
        mMainActivity = new WeakReference<>(mainActivity);
    }

    private class ServiceBroadcastReceiver extends BroadcastReceiver {
        private boolean mReceiverIsRegistered;

        public void register() {
            if (!mReceiverIsRegistered) {
                mReceiverIsRegistered = true;
                // This can't be a local broadcast as it comes from notification menu
                getApplicationContext().registerReceiver(this, new IntentFilter(NOTIFICATION_STOP));
                getApplicationContext().registerReceiver(this, new IntentFilter(TRACKING_CHANGE));
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(NOTIFICATION_STOP)) {
                if(mMainActivity.get() != null)
                    mMainActivity.get().quit();
            } else if(intent.getAction().equals(TRACKING_CHANGE)) {
                mMainActivity.get().changeCellTracking();
            }
        }
    }
}
