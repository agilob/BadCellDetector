package org.badcoders.aimsicd.utils;

import android.annotation.TargetApi;
import android.os.Build;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.badcoders.aimsicd.model.Cell;

import java.util.List;

/*
    getAllCellInfo requires API 17 and dentityWcdma.getLac() requires API 18
 */
@TargetApi(18)
public class DeviceApi18 {

    private static final String TAG = "DeviceApi18";

    public static Cell loadCellInfo(TelephonyManager tm, Cell mCell) {
        int lCurrentApiVersion = android.os.Build.VERSION.SDK_INT;
        if(mCell == null) {
            throw new IllegalArgumentException("Passed cell cannot be empty");
        }
        List<CellInfo> cellInfoList = tm.getAllCellInfo();

        if(cellInfoList != null) {
            for(final CellInfo info : cellInfoList) {
                //Network Type
                mCell.setNetType(tm.getNetworkType());
                if(info instanceof CellInfoGsm) {
                    final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
                    final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();
                    // Signal Strength
                    mCell.setDBM(gsm.getDbm()); // [dBm]
                    // Cell Identity
                    mCell.setCellId(identityGsm.getCid());
                    mCell.setMCC(identityGsm.getMcc());
                    mCell.setMNC(identityGsm.getMnc());
                    mCell.setLAC(identityGsm.getLac());

                } else if(info instanceof CellInfoCdma) {
                    final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
                    final CellIdentityCdma identityCdma = ((CellInfoCdma) info).getCellIdentity();
                    // Signal Strength
                    mCell.setDBM(cdma.getDbm());
                    // Cell Identity
                    mCell.setCellId(identityCdma.getBasestationId());
                    mCell.setMNC(identityCdma.getSystemId());
                    mCell.setLAC(identityCdma.getNetworkId());
                    mCell.setSID(identityCdma.getSystemId());

                } else if(info instanceof CellInfoLte) {
                    final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
                    final CellIdentityLte identityLte = ((CellInfoLte) info).getCellIdentity();
                    // Signal Strength
                    mCell.setDBM(lte.getDbm());
                    mCell.setTimingAdvance(lte.getTimingAdvance());
                    // Cell Identity
                    mCell.setMCC(identityLte.getMcc());
                    mCell.setMNC(identityLte.getMnc());
                    mCell.setCellId(identityLte.getCi());

                } else if(lCurrentApiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2 && info instanceof CellInfoWcdma) {
                    final CellSignalStrengthWcdma wcdma = ((CellInfoWcdma) info).getCellSignalStrength();
                    final CellIdentityWcdma identityWcdma = ((CellInfoWcdma) info).getCellIdentity();
                    // Signal Strength
                    mCell.setDBM(wcdma.getDbm());
                    // Cell Identity
                    mCell.setLAC(identityWcdma.getLac());
                    mCell.setMCC(identityWcdma.getMcc());
                    mCell.setMNC(identityWcdma.getMnc());
                    mCell.setCellId(identityWcdma.getCid());
                    mCell.setPSC(identityWcdma.getPsc());
                } else {
                    Log.w(TAG, "Unknown type of cell signal! ClassName: " + info.getClass().getSimpleName());
                }
                if(mCell.isValid())
                    break;
            }
        }

        return mCell;
    }

}
