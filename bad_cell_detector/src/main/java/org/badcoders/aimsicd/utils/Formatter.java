/*
Copyright (c) 2015-* by agilob

ISC License - The acceptable one

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD
TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/
package org.badcoders.aimsicd.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd"); //ISO8601
    private static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * Truncates GPS location to first 5 points after comma
     *
     * @param location
     *         Location value
     *
     * @return String with truncated location to 5 digits after comma
     */
    public static String formatLocation(Double location) {
        return String.format("%.5f", location);
    }

    public static Date parseDate(String date) throws ParseException {
        return DATE_FORMAT.parse(date);
    }

    /**
     * Formats date to ISO-8601 compliant format
     *
     * @return 2015-11-31 date format
     */
    public static String formatDate(Date date) {
        Log.i("Formatter", date.toString());
        return DATE_FORMAT.format(date);
    }

    public static String formatDate(Long date) {
        return DATE_FORMAT.format(new Date(date));
    }

    /**
     * Formats date to human readable date-time format
     *
     * @return 2015-11-31 15:12:12 format
     */
    public static String formatDateTime(Date date) {
        return DATE_TIME_FORMAT.format(date);
    }

    /**
     * Formats date to human readable date-time format
     *
     * @return 2015-11-31 15:12:12 format
     */
    public static String formatDateTime(Long date) {
        return DATE_TIME_FORMAT.format(new Date(date));
    }


    /**
     * Converts logcat timestamp to SQL friendly timestamps
     * We use this to determine if an sms has already been found
     *
     * @param line
     *         Converts a timestamp in this format: 06-17 22:06:05.988 D/dalvikvm(24747):
     *
     * @return Returns a timestamp as long object
     */
    public static Long logcatTimeStampParser(String line) {
        try {
            return new SimpleDateFormat("MM-dd HH:mm:ss.SSS").parse(line).getTime();
        } catch(ParseException e) {
            return new Date().getTime();
        }
    }

}
