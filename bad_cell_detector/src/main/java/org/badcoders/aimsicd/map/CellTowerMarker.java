package org.badcoders.aimsicd.map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.Formatter;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

public class CellTowerMarker extends Marker {

    private Context mContext;
    private Cell mMarkerData;

    public CellTowerMarker(Context context, MapView mapView, String aTitle,  GeoPoint aGeoPoint, Cell data) {
        super(mapView);
        mContext = context;

        mTitle = aTitle;
        mPosition = aGeoPoint;

        mMarkerData = data;
        mOnMarkerClickListener = new OnCellTowerMarkerClickListener();

        mInfoWindow = null;
    }

    public Cell getMarkerData() {
        return mMarkerData;
    }

    public View getInfoContents(Cell cell) {

        TextView tv;

        // Getting view from the layout file:  marker_info_window.xml
        View v = LayoutInflater.from(mContext).inflate(R.layout.marker_info_window, null);

        if(v != null) {
            if(cell != null) {
                tv = (TextView) v.findViewById(R.id.dbm);
                tv.setText(String.valueOf(cell.getDBM()));
                tv = (TextView) v.findViewById(R.id.lac);
                tv.setText(String.valueOf(cell.getLAC()));
                tv = (TextView) v.findViewById(R.id.lat);
                tv.setText(Formatter.formatLocation(cell.getLat()));
                tv = (TextView) v.findViewById(R.id.lng);
                tv.setText(Formatter.formatLocation(cell.getLon()));
                tv = (TextView) v.findViewById(R.id.mcc);
                tv.setText(String.valueOf(cell.getMCC()));
                tv = (TextView) v.findViewById(R.id.mnc);
                tv.setText(String.valueOf(cell.getMCC()));
            }
        }
        return v;
    }

    // This displays the Marker info pop-up dialog window, with OK button.
    private class OnCellTowerMarkerClickListener implements OnMarkerClickListener {

        @Override
        public boolean onMarkerClick(Marker marker, MapView mapView) {
            CellTowerMarker cellTowerMarker = (CellTowerMarker) marker;
            AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setTitle(cellTowerMarker.getTitle());
            dialog.setView(getInfoContents(cellTowerMarker.getMarkerData()));
            dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();

            return true;
        }
    }
}
