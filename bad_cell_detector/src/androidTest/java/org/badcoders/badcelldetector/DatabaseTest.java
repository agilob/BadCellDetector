package org.badcoders.badcelldetector;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.model.Cell;

import java.util.List;

public class DatabaseTest extends AndroidTestCase {

    private Context mContext;
    private DatabaseAdapter dbAdapter;

    @Override
     public void setUp() throws Exception {
        super.setUp();
        mContext = new RenamingDelegatingContext(getContext(), "test_");
        dbAdapter = new DatabaseAdapter(mContext);
     }

    public void testAddingCell() {
        dbAdapter = new DatabaseAdapter(mContext);

        Cell cell1 = new Cell();
        cell1.setCellId(1234);
        cell1.setLAC(5678);
        cell1.setLon(51.0d);
        cell1.setLat(12.0d);
        cell1.setPSC(1);
        cell1.setTechnology("GSM");
        cell1.setMCC(234);
        cell1.setMNC(20);
        cell1.setDBM(-88);

        long id = dbAdapter.insertCellMeasure(cell1);
        assertNotSame(-1, id);

        Cell retrievedCell = dbAdapter.getMeasureCellByCellId(1234);
        assertEquals(cell1.getCellId(), retrievedCell.getCellId());
        assertEquals(cell1.getLAC(), retrievedCell.getLAC());
        assertEquals(cell1.getLon(), retrievedCell.getLon());

        dbAdapter.deleteMeasureByCellId(1234);

        retrievedCell = dbAdapter.getMeasureCellByCellId(1234);
        assertEquals(-1, retrievedCell.getCellId());
    }

    public void testGettingAllMeasures() {
        dbAdapter = new DatabaseAdapter(mContext);

        Cell cell1 = new Cell();
        cell1.setCellId(1235);
        cell1.setLAC(5678);
        cell1.setLon(51.0d);
        cell1.setLat(12.0d);
        cell1.setPSC(1);
        cell1.setTechnology("GSM");
        cell1.setMCC(234);
        cell1.setMNC(20);
        cell1.setDBM(-88);
        long id = dbAdapter.insertCellMeasure(cell1);
        assertNotSame(-1, id);

        Cell cell2 = new Cell();
        cell2.setCellId(1235);
        cell2.setLAC(5678);
        cell2.setLon(51.5d);
        cell2.setLat(12.5d);
        cell2.setPSC(1);
        cell2.setTechnology("GSM");
        cell2.setMCC(234);
        cell2.setMNC(20);
        cell2.setDBM(-100);
        id = dbAdapter.insertCellMeasure(cell2);
        assertNotSame(-1, id);

        List<Cell> cells = dbAdapter.getAllCellMeasures();

        assertEquals(2, cells.size());
        cells.clear();

        cells = dbAdapter.getAllCellMeasuresForCell(1235);
        assertEquals(2, cells.size());
    }

}
