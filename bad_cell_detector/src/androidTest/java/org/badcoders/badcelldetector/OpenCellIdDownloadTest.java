package org.badcoders.badcelldetector;

import android.content.Context;
import android.test.InstrumentationTestCase;

import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.service.CellTracker;
import org.badcoders.aimsicd.tasks.OpenCellIdDownloadTask;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * This test is allowed to fail if OpenCellId API key is not found.
 *
 * A place to improve.
 */
public class OpenCellIdDownloadTest extends InstrumentationTestCase {

    private Context mContext;
    private DatabaseAdapter dbAdapter;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mContext = getInstrumentation().getTargetContext();
        dbAdapter = new DatabaseAdapter(getInstrumentation().getTargetContext());
    }

    public void testImportingFromOpenCellId() throws Throwable {

        final CountDownLatch signal = new CountDownLatch(1);
        CellTracker.OCID_API_KEY = "dev-usr--2c94-4fc5-9d63-714b31ab0fdb";

        class OpenCellIdDownloadTaskTest extends OpenCellIdDownloadTask {

            public OpenCellIdDownloadTaskTest(Context context, DatabaseAdapter adapter) {
                super(context, adapter);
            }
        }

        final OpenCellIdDownloadTaskTest openCellIdDownloadTask = new OpenCellIdDownloadTaskTest(mContext,
                                                                                                 dbAdapter);
        final Cell location = new Cell();
        location.setLat(51.455313);
        location.setLon(-2.591902);

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                openCellIdDownloadTask.execute(location);
            }
        });

        signal.await(10, TimeUnit.SECONDS);

        assertTrue("No networks imported!,",
                   dbAdapter.getImportedCellsByNetwork(234, 10).size() >= 0);
    }

}
